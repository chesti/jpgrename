require 'rubygems'
require 'exifr'

inputDir = ARGV[0]
prefix = ARGV[1].nil? ? "IMG" : ARGV[1]

if (ARGV.length >= 1) 
	Dir.glob("#{inputDir}/*.jpg",File::FNM_CASEFOLD).each() {|file|

		timeTaken = EXIFR::JPEG.new(file).date_time

		if (timeTaken.nil? & !EXIFR::JPEG.new(file).exif.nil?)
			timeTaken = EXIFR::JPEG.new(file).exif.date_time_original
		end
		
		if (timeTaken.nil?)
			timeTaken = File.mtime(file)
		end


		if (!timeTaken.nil?)
			filename = "#{inputDir}/#{prefix}_#{timeTaken.strftime('%Y%m%d_%H%M%S')}.JPG"

			puts "RENAME: #{file} -> #{filename}"
			if(!File.exist?(filename))
				File.rename(file,filename)
			else
				puts "ERROR: FILE #{filename} ALREADY_EXISTS!!!!"
			end
		else
			puts "ERROR: Can't get time for #{file}"
		end

	}
else
	puts "JPG_RENAME.rb #IMAGES_FOLDER [FILE_NAME_PREFIX]"
end
